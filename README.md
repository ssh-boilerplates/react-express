# React-Express boilerplate
### What you get
- **Express** server with nodemon, portfinder, helmet for both backend and catering frontend
- **React** with hot-loader and react-router
- **Webpack** with Dev/Hot middleware, **Babel** and file-loaders preconfigured
- **SCSS/node-sass** and styled-components ready
- Dev/Prod NPM scripts
- **JasmineJS with Enzyme** for complete testing (incl. React components)
- preconfigured **gitlab-ci.yml** for build and tests

## Install
```
git clone
npm install
```

## Get started
```
npm run dev
```

## Build
```
npm run build
npm start
```