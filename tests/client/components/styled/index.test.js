import jasmineCheck from 'jasmine-check'
jasmineCheck.install()


import { hello } from '../../../../client/components/styled'

describe("Can import hello function", function() {

  it("and it says Hello World!", function() {
    expect(hello()).toEqual("Hello World!")
  })
})