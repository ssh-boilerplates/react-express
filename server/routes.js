const app = require('./config');
const path = require('path')


app.get('/api', (req, res) => {
  res.send({'foo': 'bar'})
})

app.get('*.js', (req, res, next) => {
  res.set('Content-Type', 'application/javascript');
  next();
});

app.get('*.json', (req, res, next) => {
  res.set('Content-Type', 'application/json');
  next();
});

app.get('*.css', (req, res, next) => {
  res.set('Content-Type', 'text/css');
  next();
});

app.get('*', (req, res, next) => {
  res.sendFile(path.resolve('public/index.html'));
});

module.exports = app
