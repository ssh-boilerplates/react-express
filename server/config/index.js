const express = require('express');
const helmet = require('helmet')

const { NODE_ENV } = process.env;

const app = express();
app.use(helmet())

app.use(express.static('public'));

if (NODE_ENV == 'production') {
  app.use(express.static('build'));
} else {
  const webpack = require('webpack');
  const webpackConfig = require('../../webpack/dev.config');
  const compiler = webpack(webpackConfig);
  app.use(require('webpack-dev-middleware')(compiler));
  app.use(require('webpack-hot-middleware')(compiler));
}

module.exports = app