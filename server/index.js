const app = require('./routes');
const portfinder = require('portfinder');


portfinder.getPortPromise()
.then(port => {

  app.listen(port, () => console.log(`Server listening on port ${port}.`));

});
