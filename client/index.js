import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import { AppContainer } from 'react-hot-loader'

import App from './App';
import * as serviceWorker from './serviceWorker';

const render = Component => (
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('root')
  )
)

render(App)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

if (module.hot) {
  module.hot.accept('./App', () => { render(App) })
}

/* Prevent messages starting with [HMR] or [WDS] from being printed to the console
   This is a workaround used alongside the webpack-dev-server hot-module-reload feature
   It's quite chatty on the console, and there's no currently no configuration option
   to silence it. Only used in development.
 */
(function(global) {
  var console_log = global.console.log
  global.console.log = function() {
      if (!(
          arguments.length == 1 &&
          typeof arguments[0] === 'string' &&
          arguments[0].match(/^\[(HMR|WDS)\]/)
      )) {
          console_log.apply(global.console,arguments)
      }
  }
})(window)