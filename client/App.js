import React from 'react';
import { hot } from 'react-hot-loader'

function App() {
  return (
    <div className="App">
      <h3>Hello World!</h3>
    </div>
  );
}

export default hot(module)(App);